import { Component } from "react";
import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import Header from "../components/Header/Header";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      actions: [],
      isOpen: false,
      productCart: [],
      cartOrder: [],
      addToOrder: null,
      favoriteOrder: [],
    };
  }

  componentDidMount() {
    fetch("./mocup.json")
      .then((res) => res.json())
      .then((products) => {
        this.setState({ actions: products });
      });
  }

  onClose = () => {
    this.setState(() => ({ isOpen: false }));
  };

  modalSubmit = (product) => {
    this.setState({
      isOpen: true,
      addToOrder: () =>
        this.setState({
          cartOrder: [...this.state.cartOrder, product],
          isOpen: false,
        }),
    });
  };

  star = (product) => {
    // console.log(product.item)
    this.setState({
      favoriteOrder: [...this.state.favoriteOrder, product],
    });
    console.log(this.state.favoriteOrder);
  };

  removeStar = (product) => {
    const array = this.state.favoriteOrder.filter(
      (value) => value.id === product.id
    );

    this.setState({
      favoriteOrder: array,
    });
  };

  changeStars = (product) => {
  const favorite =  this.state.favoriteOrder.find((item) => {

      if (item.id === product.id) {
        return true;
        
      }

    });
    if(favorite) {
      console.log(favorite)
      this.removeStar(favorite)
    } else {
      this.star(product)
    }
  };

  render() {
    return (
      <>
        <Header favoriteOrder={this.state.favoriteOrder.length} cartOrder={this.state.cartOrder.length}>
        
        </Header>
        <ProductList
          onClick={this.modalSubmit}
          onClick2={this.star}
          actions={this.state.actions}
          change={this.changeStars}
        />

        {this.state.isOpen && (
          <Modal
            onClickSecond={() => this.state.addToOrder()}
            onClick={(e) => this.onClose()}
          />
        )}
      </>
    );
  }
}
// import { Component } from "react";
// import ProductList from "../components/ProductList/ProductList";
// import Modal from "../components/Modal/Modal";
// import Header from "../components/Header/Header";

// export default class Home extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       actions: [],
//       isOpen: false,
//       productCart: [],
//       cartOrder: [],
//       addToOrder: null,
//       favoriteOrder: [],
//       isFavourite: false,
//     };
//   }

//   componentDidMount() {
//     fetch("./mocup.json")
//       .then((res) => res.json())
//       .then((products) => {
//         this.setState({ actions: products });
//       });
//   }

//   onClose = () => {
//     this.setState(() => ({ isOpen: false }));
//   };

//   modalSubmit = (product) => {
//     this.setState({
//       isOpen: true,
//       addToOrder: () =>
//         this.setState({
//           cartOrder: [...this.state.cartOrder, product],
//           isOpen: false,
//         }),
//     });
//   };

//   star = (product) => {
//     // console.log(product)
//     const la = this.state.favoriteOrder.find((el) => el.id !== el.id);
//     this.setState({
//       favoriteOrder: [...this.state.favoriteOrder, product],
//       isFavourite: true
//     });
//     console.log(this.state.favoriteOrder);
//   };

//   render() {
//     return (
//       <>
//         <Header>
//           {this.state.cartOrder.length}
//           {this.state.favoriteOrder.length}
//         </Header>
//         <ProductList
//           onClick={this.modalSubmit}
//           onClick2={this.star}
//           actions={this.state.actions}
//           favorite={this.state.isFavourite}
//         />

//         {this.state.isOpen && (
//           <Modal
//             onClickSecond={() => this.state.addToOrder()}
//             onClick={(e) => this.onClose()}
//           />
//         )}
//       </>
//     );
//   }
// }

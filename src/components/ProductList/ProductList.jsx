import { Component } from "react";
import ProductCard from "../ProductCards/ProductCard";
import "./ProductList.scss";
import PropTypes from "prop-types";

export default class ProductList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="cards-wrapper">
        {this.props.actions.map((action) => (
          <ProductCard
            change={() => {
              this.props.change(action);
            }}
            arr={this.props.arr}
            favorite={this.props.favorite}
            onClick2={this.props.onClick2}
            onClick={this.props.onClick}
            item={action}
            key={action.id}
          />
        ))}
      </div>
    );
  }
}

ProductList.propTypes = {
  item: PropTypes.object,
};

import { Component } from "react";
import cart from "./cart-4-svgrepo-com.svg";
import star from "./star-1-svgrepo-com.svg";
import "./Header.scss";

export default class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
    
      <div className="header-wrapper">
        
          <img className="cart" src={cart} alt="" />
          {this.props.cartOrder}
      
          <img className="cart" src={star} alt="" />

          {this.props.favoriteOrder}
        {/* {this.props.children} */}
        </div>
      
    );
  }
}

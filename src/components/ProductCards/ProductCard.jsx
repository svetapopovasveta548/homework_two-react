import { Component } from "react";
import "./ProductCard.scss";
import star from "./star-1-svgrepo-com.svg";
import star2 from "./2.svg";

export default class ProductCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFavorite: false,
    };
  }

  addToFavourit = () => {
    if (this.state.isFavorite) return null;
    this.props.onClick2(this.props.item);
    this.setState({
      isFavorite: true,
    });
  };

  render() {
    return (
      <div className="card-wrapper">
        <button className="btn" onClick={this.addToFavourit}>
          <img
            className="star"
            src={this.state.isFavorite ? star2 : star}
            alt=""
          />
        </button>

        <div className="image-wrapper">
          <img className="card-image" src={this.props.item.url} alt="" />
        </div>
        <div className="info-wrapper">
          <button onClick={() => this.props.onClick(this.props.item)}>
            Add to cart
          </button>
          <div className="title">{this.props.item.title}</div>
          <div className="price">{this.props.item.price}</div>
        </div>
      </div>
    );
  }
}

ProductCard.defaultProps = {
  item: {
    url: "",
    title: "",
    price: "",
  },
};




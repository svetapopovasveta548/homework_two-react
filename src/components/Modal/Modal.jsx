import { Component } from "react";
import "./Modal.scss";
import cross from "./cross.svg";
import Button from "../Button/Button";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
    
        <div className="modal" onClick={this.props.onClick}>
          <div className="modal__wrapper" onClick={(e) => e.stopPropagation()}>
            <div className="main__wrapper">
              <div className="cross__wrapper">
                <img
                  className="cross"
                  src={cross}
                  alt=""
                  onClick={this.props.onClick}
                />
              </div>
              <p className="main__wrapper-text">Do you want to add to Cart?</p>

              <div className="button-wraper">
                <Button onClick={this.props.onClickSecond} text="Yes"></Button>
                <Button onClick={this.props.onClickThird} text="No"></Button>
              </div>
            </div>
          </div>
        </div>
      
    );
  }
}
